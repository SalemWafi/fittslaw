var user_data = { "userAgreed": Boolean, "experimentData": [] };
var distances = ["50", "150", "250", "350"];
var diameters = ["5", "10", "30", "50"];
var direction = ["right-circle", "left-circle"];
var directionChoice;
var distantChoice;
var diameterChoice;
var circleClicked = true;
var testBlocksCounter = 0;
var individualBlockCounter = 0;

// function ready to make sure the DOM elements are loaded first
$(function () {
    $("#consent").delay(600).fadeIn();

    $("#consent--btn").click(function () {
        $("#consent").addClass("fadeOutRight");
        $("#consent").delay(600).queue(function (next) {
            $(this).hide();
            next();
            $("#inst").show().addClass("fadeInLeft");
        });
        user_data.userAgreed = true;
    });

    $("#inst--btn").click(function () {
        $("#inst").addClass("fadeOutRight");
        $("#inst").delay(600).queue(function (next) {
            $(this).hide();
            next();
            $("#exp").show().addClass("fadeInLeft");
        });
    });

    $("#start-square").mouseover(function () {
        if (circleClicked) {
            directionChoice = getRndInteger(0, 2);
            distantChoice = getRndInteger(0, 4);
            diameterChoice = getRndInteger(0, 4);

            $(".circ").attr("cx", distances[distantChoice]);
            $(".circ").attr("r", diameters[diameterChoice]);
            $("#" + direction[directionChoice]).show();
            //TODO: start the stopWatch to start calculating the spent time 
            //before the end - user clicks on the circle 

            circleClicked = false;
        }
    });

    $(".circ").click(function () {
        //TODO: stop the stopWatch to calculate the time spent for 
        //him to travel from the square and click the circle

        //TODO: add the time spent for this click to the user_data JSON object 
        //to be used later for Fitts Law analysis
        circleClicked = true;


        $("#" + direction[directionChoice]).hide();
        individualBlockCounter++;
        if (individualBlockCounter == 32) {
            individualBlockCounter = 0;
            testBlocksCounter++;
            swal({
                title: "Success!",
                text: "There is " + (10 - testBlocksCounter) + " testing blocks left!",
                icon: "success",
            });
            
            if (testBlocksCounter == 10) {
                //download the json object and save it in mongoDB
                exportToJSONFile(user_data);
                setTimeout(function () { window.location.reload() }, 1000);
            };
        }
    });

    //TODO: add the event where the end-user do the mouseover event 
    // over the starting square and also he do the click event on any 
    // html element except the circle and that to record the errors that 
    // have been made by the end-user.
});

function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function exportToJSONFile(jsonData) {
    let dataStr = JSON.stringify(jsonData);
    let dataUri = 'data:application/json;charset=utf-8,' + encodeURIComponent(dataStr);

    let exportFileDefaultName = 'data.json';

    let linkElement = document.createElement('a');
    linkElement.setAttribute('href', dataUri);
    linkElement.setAttribute('download', exportFileDefaultName);
    linkElement.click();
}